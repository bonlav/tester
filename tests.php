<?php

include_once("./cases.php");

class Tests extends PHPUnit_Framework_TestCase {

  /*
  * should return the string in only uppercase letters
  */
  public function testBigLetters() {
    $result = Cases::bigLetters("tHiS iS a sEntEncE");
    $this->assertSame("THIS IS A SENTENCE", $result);
  }

  /*
  * should return only the word Bonum from the string
  */
  public function testBonumFromString() {
    $result = Cases::bonumFromString("Bonum#!!SD''l;G234EdfH");
    $this->assertSame("Bonum", $result);
  }

  /*
  * should return an array with all the words of the sentence
  */
  public function testStringToArray() {
    $result = Cases::stringToArray("This is a sentence");
    $this->assertSame(["This", "is", "a", "sentence"], $result);
  }

  /*
  * should return the number 20
  */
  public function testAddNumbers() {
    $result = Cases::addNumbers(10, "10");
    $this->assertSame(20, $result);
  }

  /*
  * should return an array with only odd numbers
  */
  public function testOnlyOddNumbers() {
    $result = Cases::onlyOddNumbers([1, 2, 3, 4, 5, 6]);
    $this->assertSame([1, 3, 5], array_values($result));
  }

  /*
  * should return an array with numbers sorted from 0-10
  */
  public function testSortNumbers() {
    $result = Cases::sortNumbers([6, 4, 9, 5, 0, 3, 1, 2, 10, 8, 7]);
    $this->assertSame([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], $result);
  }

  /*
  * should return the factorial of the number 4
  */
  public function testFactorial() {
    $result = Cases::factorial(4);
    $this->assertSame(24, $result);
  }

}
