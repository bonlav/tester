var should = require("should")
var cases = require("./cases")

describe("Kodetest", function() {

  it("should return the string in only uppercase letters", function() {
    var result = cases.bigLetters("tHiS iS a sEntEncE")
    result.should.eql("THIS IS A SENTENCE")
  })

  it("should return only the word Bonum from the string", function() {
    var result = cases.bonumFromString("Bonum#!!SD''l;G234EdfH")
    result.should.eql("Bonum")
  })

  it("should return an array with all the words of the sentence", function() {
    var result = cases.stringToArray("This is a sentence")
    result.should.eql(["This", "is", "a", "sentence"])
  })

  it("should return the number 20", function() {
    var result = cases.addNumbers(10, "10")
    result.should.be.of.type("number")
    result.should.eql(20)
  })

  it("should return an array with only odd numbers", function() {
    var result = cases.onlyOddNumbers([1, 2, 3, 4, 5, 6])
    result.should.eql([1, 3, 5])
  })

  it("should return an array with numbers sorted from 0-10", function() {
    var result = cases.sortNumbers([6, 4, 9, 5, 0, 3, 1, 2, 10, 8, 7])
    result.should.eql([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
  })

})
